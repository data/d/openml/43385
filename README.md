# OpenML dataset: Apple-(AAPL)-Historical-Stock-Data

https://www.openml.org/d/43385

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset contains Apple's (AAPL) stock data for the last 10 years (from 2010 to date). I believe insights from this data can be used to build useful price forecasting algorithms to aid investment. I would like to thank Nasdaq for providing access to this rich dataset. I will make sure I update this dataset every few months.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43385) of an [OpenML dataset](https://www.openml.org/d/43385). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43385/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43385/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43385/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

